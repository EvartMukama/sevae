import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

Future<Article> fetchArticle() async {
  final response =
      await http.get(Uri.parse('https://www.howwebiz.ug/restAPI/aticols'));

  if (response.statusCode == 200) {
    var jsonresponse = jsonDecode(response.body);
    return Article.fromJson(jsonresponse);
  } else {
    throw Exception('Loading Failed');
  }
}

class Article {
  final String title, content, subTitle, datePosted, filename, Name;
  final int views;

  Article(
      {required this.title,
      required this.content,
      required this.subTitle,
      required this.datePosted,
      required this.filename,
      required this.views,
      // ignore: non_constant_identifier_names
      required this.Name});

  factory Article.fromJson(Map<String, dynamic> json) {
    return Article(
      title: json['title'],
      content: json['content'],
      subTitle: json['subTitle'],
      datePosted: json['datePosted'],
      filename: json['filename'],
      views: json['views'] as int,
      Name: json['Name'],
    );
  }
}

String arrayObjsText =
    '{"articles": [{"name": "dart", "quantity": 12}, {"name": "flutter", "quantity": 25}, {"name": "json", "quantity": 8}]}';

var articleObjsJson = jsonDecode(arrayObjsText)['articles'] as List;
List<Article> article = articleObjsJson
    .map((articleJson) => Article.fromJson(articleJson))
    .toList();

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: const HomePage());
  }
}

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  late Future<Article> futureArticle;

  @override
  void initState() {
    super.initState();
    futureArticle = fetchArticle();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('DEASU'),
        centerTitle: true,
      ),
      body: FutureBuilder<Article>(
        future: futureArticle,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return ListView.builder(
                itemCount: 5,
                itemBuilder: (BuildContext ctx, index) {
                  return ListTile(
                    leading: Text(snapshot.data!.title),
                  );
                });
          } else if (snapshot.hasError) {
            return Text('${snapshot.error}');
          }
          return const Center(child: CircularProgressIndicator());
        },
      ),
    );
  }
}
